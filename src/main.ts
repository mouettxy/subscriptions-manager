import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import App from './App.vue'

import '@mdi/font/css/materialdesignicons.css'
import './assets/css/index.scss'
import moment from 'moment'

const i18n = createI18n({
  legacy: false,
  locale: 'en',
  fallbackLocale: 'ru',
  messages: {
    en: {
      overviewPanelItemCurrency: 'usd',
      overviewPanelItemRemaining: 'Remaining',
      overviewPanelItemTotal: 'Total',
      overviewPanelTitle: 'Overview',
      subscriptionsPanelTitle: 'Subscriptions',
      subscriptionsPanelAdd: 'Add subscription',
      subscriptionsPanel: {
        item: {
          unsubscribeText: 'Unsubscribe',
          notifications: {
            email: 'E-mail notifications',
            push: 'Push notifications',
          },
        },
      },
    },
    ru: {
      overviewPanelItemCurrency: 'rub',
      overviewPanelItemRemaining: 'Осталось',
      overviewPanelItemTotal: 'Всего',
      overviewPanelTitle: 'Обзор',
      subscriptionsPanelTitle: 'Подписки',
      subscriptionsPanelAdd: 'Добавить подписку',
      subscriptionsPanel: {
        item: {
          unsubscribeText: 'Отписаться',
          notifications: {
            email: 'Почтовые уведомления',
            push: 'Пуш уведомления',
          },
        },
      },
    },
  },
})

const app = createApp(App)

moment.locale('ru')

app.use(i18n)

app.mount('#app')
