import SIcon from '@/components/base/s-icon/s-icon.vue'
import SBtn from '@/components/base/s-btn/s-btn.vue'
import SMenu from '@/components/base/s-menu/s-menu.vue'
import SList from '@/components/base/s-list/s-list.vue'
import SListItem from '@/components/base/s-list/s-list-item/s-list-item.vue'
import SSwitch from '@/components/base/s-switch/s-switch.vue'

export { SBtn, SIcon, SMenu, SList, SListItem, SSwitch }
