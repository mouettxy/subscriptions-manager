import { ref, computed, Ref, ComputedRef } from 'vue'
import _ from 'lodash'

export type AviableColors = 'primary' | 'secondary' | 'dark' | 'light' | 'accent'

export type UseTextColor = {
  (colorName: AviableColors, asComputedArr: boolean): ComputedRef<string[]>
  (colorName: AviableColors): {
    textColor: Ref<string>
    focusTextColor: Ref<string>
    hoverTextColor: Ref<string>
  }
}

export const enum AS_COMPUTED {
  YES = 1,
  NO = 0,
}

type UseTextColorRefs = {
  textColor: Ref<string>
  focusTextColor: Ref<string>
  hoverTextColor: Ref<string>
}

type UseBgColorRefs = {
  bgColor: Ref<string>
  focusBgColor: Ref<string>
  hoverBgColor: Ref<string>
}

type UseRingColorRefs = {
  ringColor: Ref<string>
  focusRingColor: Ref<string>
  hoverRingColor: Ref<string>
}

export function useTextColor(colorName: AviableColors, asComputedArr: AS_COMPUTED.YES): ComputedRef<string[]>
export function useTextColor(colorName: AviableColors, asComputedArr: AS_COMPUTED.NO): UseTextColorRefs
export function useTextColor(
  colorName: AviableColors,
  asComputedArr: AS_COMPUTED.NO | AS_COMPUTED.YES
): ComputedRef<string[]> | UseTextColorRefs {
  const textColor = ref('')
  const focusTextColor = ref('')
  const hoverTextColor = ref('')

  const colorsMap = {
    primary: 'text-primary',
    secondary: 'text-secondary',
    dark: 'text-dark',
    light: 'text-light',
    accent: 'text-accent',
  }

  const hoverColorsMap = {
    primary: 'hover:text-primary-600',
    secondary: 'hover:text-secondary-600',
    dark: 'hover:text-dark-600',
    light: 'hover:text-light-600',
    accent: 'hover:text-accent-600',
  }

  const focusColorsMap = {
    primary: 'focus:text-primary-400',
    secondary: 'focus:text-secondary-400',
    dark: 'focus:text-dark-400',
    light: 'focus:text-light-400',
    accent: 'focus:text-accent-400',
  }

  textColor.value = colorsMap[colorName] || 'text-primary'
  focusTextColor.value = focusColorsMap[colorName] || 'focus:text-primary-400'
  hoverTextColor.value = hoverColorsMap[colorName] || 'hover:text-primary-600'

  if (asComputedArr === AS_COMPUTED.YES) {
    return toComputedArr(textColor, focusTextColor, hoverTextColor)
  }

  return {
    textColor,
    focusTextColor,
    hoverTextColor,
  }
}

export function useBgColor(colorName: AviableColors, asComputedArr: AS_COMPUTED.YES): ComputedRef<string[]>
export function useBgColor(colorName: AviableColors, asComputedArr: AS_COMPUTED.NO): UseBgColorRefs
export function useBgColor(
  colorName: AviableColors,
  asComputedArr: AS_COMPUTED.NO | AS_COMPUTED.YES
): ComputedRef<string[]> | UseBgColorRefs {
  const bgColor = ref('')
  const focusBgColor = ref('')
  const hoverBgColor = ref('')

  const colorsMap = {
    primary: 'bg-primary',
    secondary: 'bg-secondary',
    dark: 'bg-dark',
    light: 'bg-light',
    accent: 'bg-accent',
  }

  const hoverColorsMap = {
    primary: 'hover:bg-primary-600',
    secondary: 'hover:bg-secondary-600',
    dark: 'hover:bg-dark-600',
    light: 'hover:bg-light-600',
    accent: 'hover:bg-accent-600',
  }

  const focusColorsMap = {
    primary: 'focus:bg-primary-400',
    secondary: 'focus:bg-secondary-400',
    dark: 'focus:bg-dark-400',
    light: 'focus:bg-light-400',
    accent: 'focus:bg-accent-400',
  }

  bgColor.value = colorsMap[colorName] || 'bg-primary'
  focusBgColor.value = focusColorsMap[colorName] || 'focus:bg-primary-400'
  hoverBgColor.value = hoverColorsMap[colorName] || 'hover:bg-primary-600'

  if (asComputedArr === AS_COMPUTED.YES) {
    return toComputedArr(bgColor, focusBgColor, hoverBgColor)
  }

  return {
    bgColor,
    focusBgColor,
    hoverBgColor,
  }
}

export function useRingColor(colorName: AviableColors, asComputedArr: AS_COMPUTED.YES): ComputedRef<string[]>
export function useRingColor(colorName: AviableColors, asComputedArr: AS_COMPUTED.NO): UseRingColorRefs
export function useRingColor(
  colorName: AviableColors,
  asComputedArr: AS_COMPUTED.NO | AS_COMPUTED.YES
): ComputedRef<string[]> | UseRingColorRefs {
  const ringColor = ref('')
  const focusRingColor = ref('')
  const hoverRingColor = ref('')

  const colorsMap = {
    primary: 'ring-primary',
    secondary: 'ring-secondary',
    dark: 'ring-dark',
    light: 'ring-light',
    accent: 'ring-accent',
  }

  const hoverColorsMap = {
    primary: 'hover:ring-primary-600',
    secondary: 'hover:ring-secondary-600',
    dark: 'hover:ring-dark-600',
    light: 'hover:ring-light-600',
    accent: 'hover:ring-accent-600',
  }

  const focusColorsMap = {
    primary: 'focus:ring-primary-400',
    secondary: 'focus:ring-secondary-400',
    dark: 'focus:ring-dark-400',
    light: 'focus:ring-light-400',
    accent: 'focus:ring-accent-400',
  }

  ringColor.value = colorsMap[colorName] || 'ring-primary'
  focusRingColor.value = focusColorsMap[colorName] || 'focus:ring-primary-400'
  hoverRingColor.value = hoverColorsMap[colorName] || 'hover:ring-primary-600'

  if (asComputedArr === AS_COMPUTED.YES) {
    return toComputedArr(ringColor, focusRingColor, hoverRingColor)
  }

  return {
    ringColor,
    focusRingColor,
    hoverRingColor,
  }
}

const toComputedArr = (color: Ref, focus: Ref, hover: Ref): ComputedRef<string[]> => {
  return computed(() => {
    return [color.value, focus.value, hover.value]
  })
}
